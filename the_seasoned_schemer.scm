(define atom?
  (lambda (x)
    (and (not (pair? x)) (not (null? x)))))

(define one?
  (lambda (n)
    (= n 1)))

;; 11. welcome back to the show
;; the eleventh commandment
;; use additional arguments when a function needs to know what other arguments to
;; the function have been like so far

;; remember member?
(define member?
  (lambda (a lat)
    (cond
     ((null? lat) #f)
     (else
      (or (eq? a (car lat))
	  (member? a (cdr lat)))))))

;; determine wheter any atom occurs twice in a row in a list of atoms
(define is-first?
  (lambda (a lat)
    (cond
     ((null? lat) #f)
     (else
      (eq? (car lat) a)))))

(define two-in-a-row?
  (lambda (lat)
    (cond
     ((null? lat) #f)
     (else
      (or (is-first? (car lat) (cdr lat))
	  (two-in-a-row? (cdr lat)))))))

;; we can change the definitions of two-in-a-row? and is-first? in such a way that
;; two-in-a-row? leaves the decision of whether continuing the search is useful to
;; the revised version of is-first?

;; we can write a function that doesn't use two-in-a-row? at all and recur directly
;; indstead of through two-in-a-row?

(define two-in-a-row-b?
  (lambda (preceding lat)
    (cond
     ((null? lat) #f)
     (else
      (or (eq? (car lat) preceding)
	  (two-in-a-row-b? (car lat) (cdr lat)))))))

;; the natural recursion is
;; (two-in-a-row-b? (car lat) (cdr lat))
(define two-in-a-row
  (lambda (lat)
    (cond
     ((null? lat) #f)
     (else
      (two-in-a-row-b? (car lat) (cdr lat))))))

(two-in-a-row '(b d e i i a g))

(define sum-of-prefixes-b
  (lambda (sonssf tup)
    (cond
     ((null? tup) '())
     (else
      (cons
       (+ sonssf (car tup))
       (sum-of-prefixes-b (+ sonssf (car tup))
			  (cdr tup)))))))

;; obviously the first sum for sonssf mst be 0
(define sum-of-prefixes
  (lambda (tup)
    (sum-of-prefixes-b 0 tup)))


(sum-of-prefixes '(2 3 12 29 29))
(sum-of-prefixes '(1 1 1 1 1))

;; remember pick from ch4?
(define pick
  (lambda (n lat)
    (cond
     ((one? n) (car lat))
     (else
      (pick (sub1 n) (cdr lat))))))

(define scramble-b
  (lambda (tup rev-pre)
    (cond
     ((null? tup) '())
     (else
      (cons (pick (car tup)
		  (cons (car tup) rev-pre))
	    (scramble-b (cdr tup)
			(cons (car tup) rev-pre)))))))

;; we can start scramble-b with the empty list
(define scramble
  (lambda (tup)
    (scramble-b tup '())))

(scramble '(1 1 1 3 4 2 1 1 9 2))
(scramble '(1 2 3 1 2 3 4 1 8 2 10))

;; 12. take cover
;; the twelfth commandment
;; use (letrec ... ) to remove arguments that do not change for recursive applications
;;
;; the thirteenth commandment
;; use (letrec ... ) to hide and protect functions

;; remember multimember from ch3?
(define multi-remove-member
  (lambda (a lat)
    (cond
     ((null? lat) '())
     ((eq? (car lat) a)
      (multi-remove-member a (cdr lat)))
     (else
      (cons (car lat) (multi-remove-member a (cdr lat)))))))

(multi-remove-member 'tuna '(shrimp salad tuna salad and tuna))

;; remember Y from ch9?
(define Y
  (lambda (le)
    ((lambda (f)
       (f f))
     (lambda (f)
       (le
	(lambda (x)
	  ((f f) x)))))))

;; using Y we can define a recursive function without using define
((Y
 (lambda (length)
   (lambda (l)
     (cond
      ((null? l) 0)
      (else
       (+ 1 (length (cdr l))))))))
 '(1 2 3 4 5))

(define multirember
  (lambda (a lat)
    ((Y (lambda (mr)
	  (lambda (lat)
	    (cond
	     ((null? lat) '())
	     ((eq? a (car lat)) (mr (cdr lat)))
	     (else
	      (cons (car lat) (mr (cdr lat))))))))
     lat)))

(multirember 'tuna '(shrimp salad tuna salad and tuna))

;; we have another way to write this kind of definition
(define multirember
  (lambda (a lat)
    (letrec
	 ((mr (lambda (lat)
		(cond
		 ((null? lat) '())
		 ((eq? a (car lat)) (mr (cdr lat)))
		 (else
		  (cons (car lat) (mr (cdr lat))))))))
       (mr lat))))

;; mr always knows about a which doesn't change while we look through lat

(multirember 'tuna '(shrimp salad tuna salad and tuna))

(define multirember-f
  (lambda (test?)
    (letrec
	((m-f
	  (lambda (a lat)
	    (cond
	     ((null? lat) '())
	     ((test? (car lat) a)
	      (m-f a (cdr lat)))
	     (else
	      (cons (car lat) (m-f a (cdr lat))))))))
      m-f)))

((multirember-f eq?) 'tuna '(shrimp salad tuna salad and tuna))

(define multirember
  (letrec
      ((mr (lambda (a lat)
	     (cond
	      ((null? lat) '())
	      ((eq? (car lat) a) (mr a (cdr lat)))
	      (else
	       (cons (car lat) (mr a (cdr lat))))))))
    mr))

(multirember 'tuna '(shrimp salad tuna salad and tuna))

(define member?
  (lambda (a lat)
    (letrec
	((yes? (lambda (l)
		 (cond
		  ((null? l) #f)
		  ((eq? (car l) a) #t)
		  (else
		   (yes? (cdr l)))))))
      (yes? lat))))

(member? 'ice '(salad greens with pears brie cheese frozen yogurt))
(member? 'ice '(salad greens with pears brie cheese frozen yogurt and ice))

(define union
  (lambda (set1 set2)
    (cond
     ((null? set1) set2)
     ((member? (car set1) set2)
      (union (cdr set1) set2))
     (else
      (cons (car set1) (union (cdr set1) set2))))))

(union '(tomatoes and macaroni casserole) '(macaroni and cheese))

(define union
  (lambda (set1 set2)
    (letrec
	((U (lambda (set)
	      (cond
	       ((null? set) set2)
	       ((member? (car set) set2)
		(U (cdr set)))
	       (else
		(cons (car set) (U (cdr set))))))))
      (U set1))))

(union '(tomatoes and macaroni casserole) '(macaroni and cheese))

;; union knows about member but would break if members arguments change
;; we can avoid this by defining member within (letrec ... )

(define union
  (lambda (set1 set2)
    (letrec
	((U (lambda (set)
	      (cond
	       ((null? set) set2)
	       ((M? (car set) set2)
		(U (cdr set)))
	       (else
		(cons (car set) (U (cdr set)))))))
	 (M? (lambda (a lat)
	       (cond
		((null? lat) #f)
		((eq? (car lat) a) #t)
		(else
		 (M? a (cdr lat)))))))
      (U set1))))

(union '(tomatoes and macaroni casserole) '(macaroni and cheese))

;; the definition of member? inside of union ignores the twelfth commandment
;; the recursive call to M? passes along the parameter a

(define union
  (lambda (set1 set2)
    (letrec
	((U (lambda (set)
	      (cond
	       ((null? set) set2)
	       ((M? (car set) set2)
		(U (cdr set)))
	       (else
		(cons (car set) (U (cdr set)))))))
	 
	 (M? (lambda (a lat)
	       (letrec
		   ((N? (lambda (lat)
			  (cond
			   ((null? lat) #f)
			   ((eq? (car lat) a) #t)
			   (else
			    (N? (cdr lat)))))))
		 (N? lat)))))
      (U set1))))

(union '(tomatoes and macaroni casserole) '(macaroni and cheese))

;;  we can hide two-in-a-row-b? like this
(define two-in-a-row?
  (letrec
      ((W (lambda (a lat)
	    (cond
	     ((null? lat) #f)
	     (else
	      (or (eq? (car lat) a)
		  (W (car lat) (cdr lat))))))))
    (lambda (lat)
      (cond
       ((null? lat) #f)
       (else
	(W (car lat) (cdr lat)))))))

(two-in-a-row '(b d e i i a g))
(two-in-a-row '(b d e i a g))

(define sum-of-prefixes
  (lambda (tup)
    (letrec
	((S (lambda (sss tup)
	      (cond
	       ((null? tup) '())
	       (else
		(cons
		 (+ sss (car tup))
		 (S (+ sss (car tup)) (cdr tup))))))))
      (S 0 tup))))

(sum-of-prefixes '(1 1 1 1 1))

(define scramble
  (lambda (tup)
    (letrec
	((P (lambda (tup rp)
	      (cond
	       ((null? tup) '())
	       (else
		(cons (pick (car tup)
			    (cons (car tup) rp))
		      (P (cdr tup) (cons (car tup) rp))))))))
      (P tup '()))))

(scramble '(1 1 1 3 4 2 1 1 9 2))

;; 13. hop, skip, and jump
;; the fourteenth commandment
;; use (letcc.. ) to reurn values abruptly and promptly
(define intersect
  (lambda (set1 set2)
    (letrec
	((I (lambda (set)
	      (cond
	       ((null? set) '())
	       ((member? (car set) set2)
		(cons (car set) (I (cdr set))))
	       (else
		(I (cdr set)))))))
      (I set1))))

(intersect '(tomatoes and macaroni) '(macaroni and cheese))

(define intersectall
  (lambda (lset)
    (letrec
	((intersectall
	  (lambda (lset)
	    (cond
	     ((null? (cdr lset))
	      (car lset))
	     (else (intersect (car lset)
			      (intersectall (cdr lset))))))))
      (cond
       ((null? lset) '())
       (else
	(intersectall lset))))))

(intersectall '((3 mangos and)
		(3 kiwis and)
		(3 hamburgers)))

;; when there is an empty set in the list of sets, (intersectall lset) returns the empty set
(intersectall '((3 mangoes and)
		()
		(3 diet hamburgers)))

;; wouldn't it be better if intersectall didn't have to intersect each set with the
;; empty set and if it could instead say "This is it: the result is () and that's all
;; there is to it

(define intersectall
  (lambda (lset)
    (call/cc
     (lambda (hop)
	    (letrec
		((A (lambda (lset)
		      (cond
		       ((null? (car lset))
			(hop '()))
		       ((null? (cdr lset))
			(car lset))
		       (else
			(intersect (car lset) (A (cdr lset))))))))
	      (cond
	       ((null? lset) '())
	       (else
		(A lset))))))))

(intersectall '((3 mangos and)
		(3 kiwis and)
		(3 hamburgers)))

(intersectall '((3 mangoes and)
		()
		(3 diet hamburgers)))

;; we can write this using letcc

(define-syntax letcc
  (syntax-rules ()
    ((letcc var body ... )
     (call/cc
      (lambda (var) body ...)))))

(define intersectall
  (lambda (lset)
    (letcc hop
	   (letrec
	       ((A (lambda (lset)
		     (cond
		      ((null? (car lset))
		       (hop '()))
		      ((null? (cdr lset))
		       (car lset))
		      (else
		       (intersect (car lset) (A (cdr lset))))))))
	     (cond
	      ((null? lset) '())
	      (else
	       (A lset)))))))

(intersectall '((3 mangos and)
		(3 kiwis and)
		(3 hamburgers)))

(intersectall '((3 mangoes and)
		()
		(3 diet hamburgers)))
;; the fourteenth commandment
;; use (letcc.. ) to reurn values abrupt;y and promptly

;; modify intersect to skip and jump within intersectall
(define intersectall
  (lambda (lset)
    (letcc hop
	   (letrec
	       ((A (lambda (lset)
		     (cond
		      ((null? (car lset))
		       (hop '()))
		      ((null? (cdr lset))
		       (car lset))
		      (else
		       (I (car lset)
			  (A (cdr lset)))))))
		(I (lambda (s1 s2)
		     (letrec
			 ((J (lambda (s1)
			       (cond
				((null? s1) '())
				((member? (car s1) s2)
				 (cons (car s1) (J (cdr s1))))
				(else
				 (J (cdr s1)))))))
		       (cond
			((null? s2) '())
			(else
			 (J s1)))))))
		(cond
		 ((null? lset) '())
		 (else
		  (A lset)))))))

(intersectall '((3 mangos and)
		(3 kiwis and)
		(3 hamburgers)))

(intersectall '((3 mangoes and)
		()
		(3 diet hamburgers)))

;; define rember with (letrec ... )
(define rember
  (lambda (a lat)
    (letrec
	((R (lambda (lat)
	      (cond
	       ((null? lat) '())
	       ((eq? (car lat) a) (cdr lat))
	       (else
		(cons (car lat) (R (cdr lat))))))))
      (R lat))))


(rember 'tuna '(shrimp salad tuna salad and tuna))

;; the function rember-upto-last takes an atom a and a lat and removes all the
;; atoms from the lat up to and including the last occurrence of a.
;; if there are no occurrences of a, rember-upto-last returns the lat

(define rember-upto-last
  (lambda (a lat)
    (letcc skip
	   (letrec
	       ((R (lambda (lat)
		     (cond
		      ((null? lat) '())
		      ((eq? (car lat) a)
		       (skip (R (cdr lat))))
		      (else
		       (cons (car lat) (R (cdr lat))))))))
	     (R lat)))))

(rember-upto-last 'cookies
		  '(cookies chocolate mints caramel delight ginger snaps
			    desserts chocolate mousse vanilla ice cream German
			    chocolate cake more cookies gingerbreadman chocolate chip
			    brownies))

;; 14. let there be names
(define leftmost
  (lambda (l)
    (cond
     ((atom? (car l)) (car l))
     (else
      (leftmost (car l))))))

(leftmost '(((a) b) (c d)))
(leftmost '(((a) ()) () (e)))
;;(leftmost '(((() a) ())))

;; redefine leftmost to return a where l is (((() a) ()))
(define leftmost
  (lambda (l)
    (cond
     ((null? l) '())
     ((atom? (car l)) (car l))
     (else
      (cond
       ((atom? (leftmost (car l)))
	(leftmost (car l)))
       (else
	(leftmost (cdr l))))))))

(leftmost '((() a) ()))

;; the repetition of (leftmost (car l)) seems wrong

(define leftmost
  (lambda (l)
    (cond
     ((null? l) '())
     ((atom? (car l)) (car l))
     (else
      (let ((a (leftmost (car l))))
	(cond
	 ((atom? a) a)
	 (else
	  (leftmost (cdr l)))))))))

(leftmost '((() a) ()))

;; the fifteenth commandment (preliminary version)
;; use (let ...) to name the values of repeated expressions

(define rember1*
  (lambda (a l)
    (cond
     ((null? l) '())
     ((atom? (car l))
      (cond
       ((eq? (car l) a) (cdr l))
       (else
	(cons (car l) (rember1* a (cdr l))))))
     (else
      (cond
       ((equal? (rember1* a (car l)) (car l))
	(cons (car l) (rember1* a (cdr l))))
       (else
	(cons (rember1* a (car l)) (cdr l))))))))

(rember1* 'salad '((Swedish rye)
		   (French (mustard salad turkey))
		   salad))

(define rember1*
  (lambda (a l)
    (letrec
	((R (lambda (l)
	      (cond
	       ((null? l) '())
	       ((atom? (car l))
		(cond
		 ((eq? (car l) a) (cdr l))
		 (else
		  (cons (car l)
			(R (cdr l))))))
	       (else
		(let ((av (R (car l))))
		  (cond
		   ((equal? (car l) av)
		    (cons (car l) (R (cdr l))))
		   (else
		    (cons av (cdr l))))))))))
      (R l))))

(rember1* 'salad '((Swedish rye)
		   (French (mustard salad turkey))
		   salad))

(rember1* 'salad '((Swedish rye)
		   (French (mustard turkey))))

;; define depth* using let and if
;;
(define depth*
  (lambda (l)
    (cond
     ((null? l) 1)
     ((atom? (car l))
      (depth* (cdr l)))
     (else
      (let ((a (+ 1 (depth* (car l))))
	    (d (depth* (cdr l))))
	(if (> d a) d a))))))

(depth* '(c (b (a b) a) a))

;; whats a good name for (lambda (n m) (if (> n m) n m))?
;; max, because the function selects the larger of two numbers

(define depth*
  (lambda (l)
    (cond
     ((null? l) 1)
     ((atom? (car l))
      (depth* (cdr l)))
     (else
      (max (+ 1 (depth* (car l))) (depth* (cdr l)))))))

(depth* '(c (b (a b) a) a))


;; the fifteenth commandment (revised version)
;; the (let ...) to name the values of repeated expressions
;; in a function definition if they may be evaluated twice
;; for one and the same use of the function

(depth* '(()
	  ((bitter butter)
	   (makes)
	   (batter (bitter))
	   butter)))

(depth* '(c (b (a b) a) a))

;; scramble from 12. using let
(define scramble
  (lambda (tup)
    (letrec
	((P (lambda (tup rp)
	      (cond
	       ((null? tup) '())
	       (else
		(let ((rp (cons (car tup) rp)))
		  (cons (pick (car tup) rp) (P (cdr tup) rp))))))))
      (P tup '()))))

(scramble '(1 1 1 3 4 2 1 1 9 2))

;; back to leftmost
;; 
(define leftmost
  (lambda (l)
    (cond
     ((null? l) '())
     ((atom? (car l)) (car l))
     (else
      (let ((a (leftmost (car l))))
	(cond
	 ((atom? a) a)
	 (else
	  (leftmost (cdr l)))))))))

;; what is (leftmost l) where l is (((a)) b (c))
(leftmost '(((a)) b (c)))

;; because we are recuring within let, when we hit the terminating condition
;; we still set a to 'a, check that that is is an atom one more time, and then we're completely done

(let ((a '(((a)))))
  (let ((a '((a))))
    ; we know that the value of l is an atom at this point, but leftmost was called within the previous let, where 'a is returned to
    (let ((a '(a))) ; we set the value to a
      a))) ;; we then check if a is atom again, and then return

;; we can fix this with letcc

(define leftmost
  (lambda (l)
    (letcc skip
	   (letrec
	       ((lm (lambda (l)
		      (cond
		       ((null? l) '())
		       ((atom? (car l))
			(skip (car l))) ;; jump to skip
		       (else
			(let ()
			  (lm (car l))
			  (lm (cdr l))))))))
	     (lm l)))))

(leftmost '(((a)) b (c)))

;; the function lm looks at every atom in l from left to right
;; until it finds an atom and then uses skip to return this
;; atom abruptly and promptly

;; when the skip procedure is invoked within a recuring call
;; the atom no is returned and set to new-car. the recursion continues with the value of l
(define rm
  (lambda (a l oh)
    (cond
     ((null? l) (oh 'no))
     ((atom? (car l))
      (if (eq? (car l) a) (cdr l)
	  (cons (car l) (rm a (cdr l) oh))))
     (else
      (let
	  ((new-car (letcc oh
			   (rm a (car l) oh))))
	(if (atom? new-car)
	    (cons (car l) (rm a (cdr l) oh))
	    (cons new-car (cdr l))))))))

(rm 'salad '((Swedish rye)
		   (French (mustard salad turkey))
		   salad)
    (lambda (x) '()))


(define rember1*
  (lambda (a l)
    (let
	((new-l (letcc oh
		       (rm a l oh))))
      (if (atom? new-l) l new-l))))

(rember1* 'noodles '())
(rember1* 'noodles '(()))
(rember1* 'noodles '(food))
(rember1* 'noodles '(food noodles))

;; we can protect rm
(define rember1*
  (lambda (a l)
    (letrec
	((r (lambda (a l oh)
		(cond
		 ((null? l) (oh 'no))
		 ((atom? (car l))
		  (if (eq? (car l) a) (cdr l)
		      (cons (car l) (r a (cdr l) oh))))
		 (else
		  (let
		      ((new-car
			(letcc oh
			       (r a (car l) oh))))
		    (if (atom? new-car)
			(cons (car l) (r a (cdr l) oh))
			(cons new-car (cdr l)))))))))
      (let ((new-l (letcc oh
			  (r a l oh))))
	  (if (atom? new-l) l new-l)))))

(rember1* 'noodles '())
(rember1* 'noodles '((noodles) more (food)))
(rember1* 'noodles '(food noodles))

(define-syntax try
  (syntax-rules ()
    ((try var a . b)
     (letcc success
	    (letcc var (success a)) . b))))

;; we can simplify rm with (try)

(define rember1*
  (lambda (a l)
    (let
	((rm  (lambda (a l oh)
		(cond
		 ((null? l) (oh 'no))
		 ((atom? (car l))
		  (if (eq? (car l) a) (cdr l)
		      (cons (car l) (rm a (cdr l) oh))))
		 (else
		  (try oh2
		       (cons (rm a (car l) oh2) (cdr l))
		       (cons (car l) (rm a (cdr l) oh))))))))
      (try oh (rm a l oh) l))))

(rember1* 'noodles '())
(rember1* 'noodles '(food))
(rember1* 'noodles '(food noodles))

;; 15. the difference between men and boys
;; the sixteenth commandment
;; use (set! ...) only with names defines in (let ...)s.
;;
;; the seventeenth commandment
;; use (set! x ...) for (let ((x ...)) ...) only if
;; there is at least one (lambda ... between it
;; and the (let ((x ...)) ...).
;;
;; the eighteenth commandment
;; use (set! x ...) only when the value that x
;; refers to is no longer needed.
(set! x 'gone)

;; set bang doesn't return a value, but the effect is as if we had just written
(define x 'gone)

;; more examples

(define gourmand
  (lambda (food)
    (set! x food)
    (cons food
	  (cons x '()))))

(gourmand 'potato)

(define dinerR
  (lambda (food)
    (set! x food)
    (cons 'milkshake
	  (cons food '()))))

(dinerR 'pecanpie)
x ; peacanpie

(gourmand 'onion)
x ; onion

;; both dinerR and gourmand use x to remember the food they saw last
;; we could have used different names but we could have the same problem again

(define omnivore
  (let ((x 'minestrone))
    (lambda (food)
      (set! x food)
      (cons food
	    (cons x '())))))

(omnivore 'bouillabaisse)


;; the definition of omnivore is almost like writing two definitions:

(define x 'minestrone)
(define omnivore
  (lambda (food)
    (set! x food)
    (cons food
	  (cons x '()))))

;; but is really is
(define __x 'minestrone)
(define omnivore
  (lambda (food)
    (set! __x food)
    (cons food
	  (cons x '()))))

;; __x is imaginary it will never be used with (define ... ) again

(define nibbler
  (lambda (food)
    (let ((x 'donut))
      (set! x food)
      (cons food
	    (cons x '())))))

(nibbler 'cheerio)

;; nibbler doesn't know about x, it has established new scope

;; if (let ...) and (set! ...) are used without a (lambda .. between them, they
;; don't help us to remember things

;; the seventeenth commandment
;; use (set! x ...) for (let ((x ...)) ...) only if
;; there is at least one (lambda ... between it
;; and the (let ((x ...)) ...).

;; the eighteenth commandment
;; use (set! x ...) only when the value that x
;; refers to is no longer needed.

(define food 'garlic)

(define x 'potato)

food ;garlic

x ;potato

(define chez-nous
  (lambda ()
    (let ((a food))
      (set!  food x)
      (set! x a))))

(chez-nous)

food ; potato

x ;garlic

;; (let ...) names values. if chez-nous first
;; names the value in food, we have two ways
;; to refer to its value. and we can use the
;; name in (let ...) to put this value into x.

;; 16. ready, set, bang!
;; the nineteenth commandment
;; use (set! ...) to remember valuable things between two
;; distinct uses of a function (memoize)

;; the seventeenth commandment (final version)
;; use (set! x ...) for (let ((x ... )) ... ) only if there is at least
;; one (lambda ... between it and the (let ...), or if the new value for
;; x is a function that refers to x

(define last 'angelfood)
(define sweet-tooth
  (lambda (food)
    (cons food
	  (cons 'cake '()))))

(sweet-tooth 'chocolate)

last ; anglefood

(sweet-tooth 'fruit)

last ; anglefood

(define sweet-toothL
  (lambda (food)
    (set! last food)
    (cons food (cons 'cake '()))))

(sweet-toothL 'chocolate)

last ;chocolate

(sweet-toothL 'fruit)

last ;fruit

(sweet-toothL 'cheese)

last ;cheese

(define ingredients '())
(define sweet-toothR
  (lambda (food)
    (set! ingredients
      (cons food ingredients))
    (cons food (cons 'cake '()))))

(sweet-toothR 'chocolate)

ingredients ;chocolate

(sweet-toothR 'fruit)

ingredients

(sweet-toothR 'carrot)

ingredients


(define deep
  (lambda (m)
    (cond
     ((zero? m) 'pizza)
     (else
      (cons (deep (sub1 m)) '())))))

(deep 3) ; (((pizza)))


(define Rs '())
(define Ns '())
(define deepR
  (lambda (n)
    (let ((result (deep n)))
      (set! Rs (cons result Rs))
      (set! Ns (cons n Ns))
      result)))

(deepR 3) ; (((pizza)))

Ns ; (3)
Rs ; (((pizza)))

;; the nineteenth commandment
;; use (set! ...) to remember valuable things between two
;; distinct uses of a function

(deepR 3) ; (((pizza)))
Ns ; (3 3)
Rs ; ((((pizza))) (((pizza))))

;; we already know the result, we can look inside Ns and Rs

(define find
  (lambda (n Ns Rs)
    (letrec
	((A (lambda (ns rs)
	      (cond
	       ((null? ns) #f)
	       ((= (car ns) n) car rs)
	       (else
		(A (cdr ns) (cdr rs)))))))
      (A Ns Rs))))

(find 3 '(3) '(((3)))')
(find 3 '(3) 'found)
(find 3 '() '()) ; page 117.

;; deepM is like deepR but avoids unnecessary consing onto Ns

(define deepM
  (lambda (n)
    (if (member? n Ns)
	(find n Ns Rs)
	(deepR n))))

;; without using deepR

(define deepM
  (lambda (n)
    (if (member? n Ns)
	(find n Ns Rs)
	(let ((result (deep n)))
	  (set! Rs (cons result Rs))
	  (set! Ns (cons n Ns))
	  result))))

(deepM 9)

;; deep using deepM

(define deep
  (lambda (m)
    (cond
     ((zero? m) 'pizza)
     (else
      (cons (deepM (sub1 m)) '())))))

(deepM 9)

;; we can replace memeber? with find since find handles the case when
;; its second argument is empty

(define deepM
  (let ((Rs '())
	(Ns '()))
    (lambda (n)
      (let ((exists (find n Ns Rs)))
	(if (atom? exists)
	    (let ((result (deep n)))
	      (set! Rs (cons result Rs))
	      (set! Ns (cons n Ns))
	      result)
	    exists)))))

(deepM 9)

;; remember length?

(define length
  (lambda (l)
    (cond
     ((null? l) 0)
     (else
      (add1 (length (cdr l)))))))

(length  '(1 2 3 4 5))

(define length
  (let ((h (lambda (l) 0)))
    (set! h
      (lambda (l)
	(cond
	 ((null? l) 0)
	 (else
	  (add1 (h (cdr l)))))))
    h))

(length  '(1 2 3 4 5))

;; the seventeenth commandment (final version)
;; use (set! x ...) for (let ((x ... )) ... ) only if there is at least
;; one (lambda ... between it and the (let ...), or if the new value for
;; x is a function that refers to x

;; evaluating the definition creats an imaginary definition for
;; h by removing it from the (let ...)

(lambda (l)
  (cond
   ((null? l) 0)
   (else
    (add1
     ((lambda (arg) (h arg)) (cdl)))))) ; we replace (lambda (arg) (h arg)) with the imaginary definition of h

(define L
  (lambda (length)
    (lambda (l)
      (cond
       ((null? l) 0)
       (else (add1 (length (cdr l))))))))

(define y!
  (lambda (L)
    (let ((h (lambda (l) '())))
      (set! h
	(L (lambda (arg) (h arg))))
      h)))

((y! L) '(1 2 3 4 5))

;; a (letrec ...) is an abbreviation for an expression consisting of (let ...)
;; and (set! ...)

(define y-bang
  (lambda (f)
    (letrec
	((h (f (lambda (arg) (h arg)))))
      h)))

((y-bang L) '(1 2 3 4 5))

;; this is "the applicative-order imperative Y combinator"
;; thank you, Peter J. Landin.

(define D
  (lambda (depth*)
    (lambda (s)
      (cond
       ((null? s) 1)
       ((atom? (car s)) (depth* (cdr s)))
       (else
	(max (add1 (depth* (car s)))
	     (depth* (cdr s))))))))

(define depth* (y-bang D))

(depth* '(c (b (a b) a) a))

;; 17. we change, therefore we are!

;; we can replace the call to deep using (if ...)
(define deepM
  (let ((Rs '())
	(Ns '()))
    (lambda (n)
      (let ((exists (find n Ns Rs)))
	(if (atom? exists)
	    (let ((result ((lambda (m)
			     (if (zero? m)
				 'pizza
				 (cons (deepM (sub1 m)) '())))
			   n)))
	      (set! Rs (cons result Rs))
	      (set! Ns (cons n Ns))
	      result)
	    exists)))))

(deepM 9)

(define deepM
  (let ((Rs '())
	(Ns '()))
    (letrec
	((D (lambda (m)
	      (if (zero? m) 'pizza
		  (cons (D (sub1 m)) '())))))
      (lambda (n)
	(let ((exists (find n Ns Rs)))
	  (if (atom? exists)
	      (let ((result (D n)))
		(set! Rs (cons result Rs))
		(set! Ns (cons n Ns))
		result)
	      exists))))))

(deepM 9)

;; deepM works and it can be simplified
;; (let ... ) can be used because Ns and Rs do not appear in the definition of D
;; but since the definition does not contain (set! D ...) and D is used in only one place,
;; we can replace D by its value

(define deepM
  (let ((Rs '())
	(Ns '()))
    (lambda (n)
      (let ((exists (find n Ns Rs)))
	(if (atom? exists)
	    (let ((result (if (zero? n) 'pizza
			      (cons (deepM (sub1 n)) '()))))
	      (set! Rs (cons result Rs))
	      (set! Ns (cons n Ns))
	      result)
	    exists)))))

(deepM 9)

;; consC returns the same value as cons and counts how many times it sees arguments
(define consC
  (let ((N 0))
    (lambda (x y)
      (set! N (add1 N))
      (cons x y))))

(define deep
  (lambda (m)
    (if (zero? m) 'pizza
	(consC (deep (sub1 m)) '()))))

;; how to access N?
(define set-counter)
(define counter)
(define consC
  (let ((N 0))
    (set! counter (lambda () N))
    (set! set-counter (lambda (x) (set! N x)))
    (lambda (x y)
      (set! N (add1 N))
      (cons x y))))

(counter)

(deep 5)
(counter)
(deep 7)
(counter)

(define supercounter
  (lambda (f)
    (letrec
	((S (lambda (n)
	      (if (zero? n) (f n)
		  (let ()
		    (f n)
		    (S (sub1 n)))))))
      (S 1000)
      (counter))))

(supercounter deep)
(set-counter 0)

(define deepM
  (let ((Rs '())
	(Ns '()))
    (lambda (n)
      (let ((exists (find n Ns Rs)))
	(if (atom? exists)
	    (let ((result (if (zero? n) 'pizza
			      (consC (deepM (sub1 n)) '()))))
	      (set! Rs (cons result Rs))
	      (set! Ns (cons n Ns))
	      result)
	    exists)))))

(set-counter 0)
(deep 7)
(counter)

(supercounter deepM)

;; deep uses 499, 500 more conses to return the same value as deepM
;; "A LISP programmer knows the value of everything but the cost of nothing"
;; thank you, Alan J. Perlis (1922-1990)

(set-counter 0)

(define rember1*C
  (lambda (a l)
    (letrec
	((R (lambda (l oh)
	      (cond
	       ((null? l) (oh 'no))
	       ((atom? (car l)) (if (eq? (car l) a)
				    (cdr l)
				    (consC (car l) (R (cdr l) oh))))
	       (else
		(let ((new-car
		       (letcc oh
			      (R (car l) oh))))
		  (if (atom? new-car)
		      (consC (car l) (R (cdr l) oh))
		      (consC new-car (cdr l)))))))))
      (let ((new-l (letcc oh (R l oh))))
	(if (atom? new-l)
	    l
	    new-l)))))

(rember1*C 'noodles '((food) more (food)))
(counter)
;; the value of (counter) is 0
;; we always use a compass needle and the North Pole to get rid of pending consCes.

(define rember1*C2
  (lambda (a l)
    (letrec
	((R (lambda (l)
	      (cond
	       ((null? l) '())
	       ((atom? (car l)) (if (eq? (car l) a)
				    (cdr l)
				    (consC (car l) (R (cdr l)))))
	       (else
		(let ((av (R (car l))))
		  (if (equal? (car l) av)
		      (consC (car l) (R (cdr l)))
		      (consC av (cdr l)))))))))
      (R l))))

(set-counter 0)
(rember1*C2 'noodles '((food) more (food)))
(counter)

;; the value of (counter) is 5
;; rember1*C2 needs five consCs to rebuild the list ((food) more (food))

;; 18. we change, therefore we are the same!

(define lots
  (lambda (m)
    (cond
     ((zero? m) '())
     (else
      (consC 'egg (lots (sub1 m)))))))

(lots 3) ; (egg, egg, egg)
(lots 5) ; (egg, egg, egg, egg, egg)
(length (lots 3)) ; 3
(length (lots 5)) ; 5

;; add item at other end of the list
(define add-at-end
  (lambda (l)
    (cond
     ((null? (cdr l))
      (consC (car l)
	     (cons 'egg '())))
     (else
      (consC (car l) (add-at-end (cdr l)))))))

(set-counter 0)
(add-at-end '(a b c)) ; (a, b, c, egg)
(counter) ; 3

;; can we add an item to the end of the list with only one cons?
(define add-at-end-too
  (lambda (l)
    (letrec
	((A (lambda (ls)
	      (cond
	       ((null? (cdr ls))
		(set-cdr! ls (consC 'egg '())))
	       (else
		(A (cdr ls)))))))
      (A l)
      l)))

(set-counter 0)
(add-at-end-too '(a b c)) ; (a, b, c, egg)
(counter) ; 1

;; remember zub1, edd1 and sero? from the little schemer. we can
;; approximate cons in a similar way

(define kons
  (lambda (kar kdr)
    (lambda (selector)
      (selector kar kdr))))

(define kar
  (lambda (c)
    (c (lambda (a d) a))))

(define kdr
  (lambda (c)
    (c (lambda (a d) d))))

(define lotsb
  (lambda (m)
    (cond
     ((zero? m) '())
     (else
      (kons 'egg
	    (lotsb (sub1 m)))))))

(define lenkth
  (lambda (l)
    (cond
     ((null? l) 0)
     (else
      (add1 (lenkth (kdr l)))))))

(kdr (lotsb 2))
(lenkth (lotsb 2))

(define bons
  (lambda (kar)
    (let ((kdr '()))
      (lambda (selector)
	(selector
	 (lambda (x) (set! kdr x))
	 kar
	 kdr)))))

(bons 'egg)

(define kar
  (lambda (c)
    (c (lambda (s a d) a))))

(define kdr
  (lambda (c)
    (c (lambda (s a d) d))))

;; we can define a selector to change the value that kdr in bons refers to
(define set-kdr
  (lambda (c x)
    ((c (lambda (s a d) s)) x)))

;;using set-kdr we can define kons
(define kons
  (lambda (a d)
    (let ((c (bons a)))
      (set-kdr c d)
      c)))

(kar (kons 'a '())) ; a
(kdr (kons 'a '())) ; ()
(kdr (kons 'a (kons 'b '()))) ; selector function for tail

(equal? (kdr (kons 'a '())) (kdr (kons 'a '())))

;;(define ab (kdr (kons 'a (kons 'b '()))))
;;(set-kdr ab (kons 'c '()))
;;(set-kdr ab (kons 1 '()))
;;(kar (kdr ab))


(set-counter 0)
(define dozen (lots 12))
(counter) ; 12

(define bakers-dozen (add-at-end dozen))
(length bakers-dozen) ; 13

(counter)

(define bakers-dozen-too (add-at-end-too dozen))

(length bakers-dozen-too) ; 13

(define eklist?
  (lambda (ls1 ls2)
    (cond
     ((null? ls1) (null? ls2))
     ((null? ls2) #f)
     (else
      (and (eq? (car ls1) (car ls2))
	   (eklist? (cdr ls1) (cdr ls2)))))))

;; conses in bakers-dozen are the same as all of bakers-dozen-too
(eklist? bakers-dozen bakers-dozen-too) ; #t
(eklist? (cons 'a (cons 'b '())) (cons 'a (cons 'b '()))) ; #t
(eklist? (cons 'a (cons 'b '())) (cons 'a '())) ; #f

;; two lists are the same if for each item in ls1, ls2 has the same
;; item in the same position
;;
;; how to define "the same" is a deep philosophical question
;; thank you, Gottfried W. Leibniz (1646 - 1716)

;; there is a new idea of "sameness" once we introduce (set! ..)

(define same?
  (lambda (c1 c2)
    (let ((t1 (cdr c1))
	  (t2 (cdr c2)))
      (set-cdr! c1 1)
      (set-cdr! c2 2)
      (let ((v (= (cdr c1) (cdr c2))))
	(set-cdr! c1 t1)
	(set-cdr! c2 t2)
	v))))

;; thank you, Gerald J. Sussman and Guy L. Steele Jr.

(same? dozen bakers-dozen-too) ; #t
(same? (cons 'egg '()) (cons 'egg '())) ; #f

(define last-cons
  (lambda (ls)
    (cond
     ((null? (cdr ls)) ls)
     (else
      (last-cons (cdr ls))))))

(define long (lots 12))

(last-cons long) ; egg

;; length again
(define length
  (let ((h (lambda (l) 0)))
    (set! h
      (lambda (l)
	(cond
	 ((null? l) 0)
	 (else
	  (add1 (h (cdr l)))))))
    h))

(length long) ; 12

(set-cdr! (last-cons long) long)
;;last-cons

;; setting the last-cons to long creats an infinate loop in the list

;;(length long)
;;long

;; length cannot terminate becuae no cdr refers to the empty list, the only
;; one that did was changed we can use Floyd's cycle-finding algorithm to detect this

(define finite-length
  (lambda (p)
    (letcc infinate
	   (letrec
	       ((C (lambda (p q)
		     (cond
		      ((null? q) 0)
		      ((null? (cdr q)) 1)
		      ((same? p q) (infinate #f))
		      (else
		       (+ (C (sl p) (qk q))
			  2)))))
		(qk (lambda (x) (cdr (cdr x))))
		(sl (lambda (x) (cdr x))))
	     (cond
	      ((null? p) 0)
	      (else
	       (add1 (C p (cdr p)))))))))

(finite-length '(a b))
(finite-length long)

;; 19. absconding with the jewels

(define toppings)
(define deepB
  (lambda (m)
    (cond
     ((zero? m)
      (letcc jump
	     (set! toppings jump)
	     'pizza))
     (else
      (cons (deepB (sub1 m)) '())))))

(deepB 6)
(toppings 'cake)

;; let's add another layer to the cake
(cons (toppings 'cake) '()) ; ((((((cake))))))

;; there are still 6 toppings, the first cons has been forgotten

;; let's add three slices to the mozzarella
(cons
 (cons
  (cons (toppings 'mozzarella) '())
  '())
 '()) ; ((((((mozzarella))))))

;; whenever we use (toppings m) it forgets everything surrounding it
;; and adds exactly six layers of parentheses

(deepB 4) ; ((((pizza))))
(cons
 (cons
  (cons (toppings 'mozzarella) '())
  '())
 '()) ; ((((mozzarella))))

(cons (toppings 'cake)
      (toppings 'cake)) ; ((((cake))))

;; when we use a value made with (letcc ... ) it forgets everything around it

(define deep&co
  (lambda (m k)
    (cond
     ((zero? m) (k 'pizza))
     (else
      (deep&co (sub1 m)
	       (lambda (x)
		 (k (cons x '()))))))))

(deep&co 0 (lambda (x) x))
(deep&co 6 (lambda (x) x))

(define deep&coB
  (lambda (m k)
    (cond
     ((zero? m)
      (let ()
	(set! toppings k)
	(k 'pizza)))
     (else
      (deep&coB (sub1 m)
		(lambda (x)
		  (k (cons x '()))))))))

(deep&coB 4 (lambda (x) x)) ; ((((cake))))
(toppings 'cake) ; ((((cake))))

;; the final collector is related to the function that is equivalent to the
;; one created with (letcc ... ) in deepB

(cons (toppings 'cake)
      (toppings 'cake)) ; (((((cake)))) ((((cake)))))

;; this version of toppings would not forget everything

(cons (toppings 'cake)
      (cons (toppings 'mozzarella)
	    (cons (toppings 'pizza) '()))) ; (((((cake)))) ((((mozzarella)))) ((((pizza)))))

;; beware of shadows!
;; shadows are close to the real thing, but we should not forget the
;; difference between them and the real thing

(define leave)
(define walk
  (lambda (l)
    (cond
     ((null? l) '())
     ((atom? (car l)) (leave (car l)))
     (else
      (let()
	(walk (car l))
	(walk (cdr l)))))))

(define start-it
  (lambda (l)
    (letcc here
	   (set! leave here)
	   (walk l))))

;; the function start-it sets up a north pole in 'here', remembers it in leave, and then determines
;; the value of (walk l). the function walks crawls over l from left to right until it finds an atom
;; and then uses leave to return that atom as the value of (start-it l)
(start-it '((potato (chips (chips (with)))) fish))

(define fill)
(define waddle
  (lambda (l)
    (cond
     ((null? l) '())
     ((atom? (car l))
      (let ()
	(letcc rest
	       (set! fill rest)
	       (leave (car l)))
	(waddle (cdr l))))
     (else
      (let ()
	(waddle (car l))
	(waddle (cdr l)))))))

(define start-it2
  (lambda (l)
    (letcc here
	   (set! leave here)
	   (waddle l))))

(start-it2 '((donuts)
	     (cheerios (cheerios (spaghettios)))
	     donuts)) ; donuts

(fill 'go)
(fill 'go)
(fill 'go)
(fill 'go)

(define get-next
  (lambda (x)
    (letcc here-again
	   (set! leave here-again)
	   (fill 'go))))

(define get-first
  (lambda (l)
    (letcc here
	   (set! leave here)
	   (waddle l)
	   (leave '()))))

(get-first '(donut)) ; donut
(get-next 'go) ; ()

(get-first '(fish (chips))) ; fish
(get-next 'go) ; chips

(get-first '(fish (chips) chips)) ; fish
(get-next 'go) ; chips
(get-next 'go) ; chips
(get-next 'go) ; ()

(define two-in-a-row*?
  (lambda (l)
    (let ((fst (get-first l)))
      (if (atom? fst)
	  (two-in-a-row-b*? fst)
	  #f))))

(define two-in-a-row-b*?
  (lambda (a)
    (let ((n (get-next 'go)))
      (if (atom? n)
	  (or (eq? n a)
	      (two-in-a-rob-b*? n))
	  #f))))

(two-in-a-row*? '(((food) ()) (((food))))) ; #t

(define two-in-a-row*?
  (letrec
      ((T? (lambda (a)
	     (let ((n (get-next 0)))
	       (if (atom? n)
		   (or (eq? n a)
		       (T? n))
		   #f))))
       (get-next
	(lambda (x)
	  (letcc here-again
		 (set! leave here-again)
		 (fill 'go))))
       (fill (lambda (x) x))
       (waddle
	(lambda (l)
	  (cond
	   ((null? l) '())
	   ((atom? (car l))
	    (let ()
	      (letcc rest
		     (set! fill rest)
		     (leave (car l)))
	      (waddle (cdr l))))
	   (else
	    (let ()
	      (waddle (car l))
	      (waddle (cdr l)))))))
       (leave (lambda (x) x)))
    (lambda (l)
      (let ((fst (letcc here
			(set! leave here)
			(waddle  l)
			(leave '()))))
	(if (atom? fst)
	    (T? fst)
	    #f)))))

(two-in-a-row*? '(((food) ()) (((food))))) ; #t
(two-in-a-row*? '(((food) ()) (((foood))))) ; #t
