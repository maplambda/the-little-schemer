;; 1. toys
;; quoting, car, list, s-expression

;; 2. do it, do it again, and again, and again...
;; always ask null? as the first question in expressing any function
(define lat?
  (lambda (l)
    (cond
     ((null? l) #t)
     ((atom? (car l)) (lat? (cdr l)))
     (else #f))))

(define member?
  (lambda (a lat)
    (cond
     ((null? lat) #f)
     (else (or (eq? (car lat) a)
	       (member? a (cdr lat)))))))

;; 3. cons the magnificent
;; use cons to build lists
(define remove-member
  (lambda (a lat)
    (cond
     ((null? lat) '())
     ((eq? (car lat) a) (cdr lat))
     (else (cons (car lat)
		 (remove-member a (cdr lat)))))))

(remove-member 'x '(a b c)) ; (a b c)
(cons 'a (cons 'b (cons 'c '()))) ; (a b c)

(remove-member 'b '(a b c)) ; (a c)
(cons 'a '(c)) ; (a c)

;; when building a list, describe the first typical element, and then cons it onto the natural recursion
(define firsts 
  (lambda (l)
    (cond
     ((null? l) '())
     (else (cons (car (car l))
		 (firsts (cdr l)))))))

(firsts '((apple peach pumpkin)
	  (plum pear cherry)
	  (grape raisin pea)
	  (bean carrot eggplant))) ; (apple plum grape bean)

(define insertR
  (lambda (new old lat)
    (cond
     ((null? lat) '())
     (else (cond
	    ((eq? (car lat) old)
	     (cons old (cons new (cdr lat))))
	    (else
	     (cons (car lat)
		   (insertR new old (cdr lat)))))))))

(insertR 'topping 'fudge '(ice cream with fudge for dessert)); (ice cream with fudge topping for dessert)
(insertR 'jalapeno 'and '(tacos tamales and salsa)) ;(tacos tamales and jalapeno salsa)
(insertR 'e 'd '(a b c d f g d h)) ;(a b c d e f g d h)

(define insertL
  (lambda (new old lat)
    (cond
     ((null? lat) '())
     (else
      (cond
       ((eq? (car lat) old)
	(cons new lat))
       (else
	(cons (car lat) (insertL new old (cdr lat)))))))))

(insertL 'b 'c '(a c d))

;; subst2 replaces either the first occurance of o1 or the first occurence of o2 by new
;; where
;;  new is vanilla
;;  o1 is chocolate
;;  o2 is banana
;; and lat is (banana ice cream with chocolate topping)
;; the value is (vanilla ice cream with chocolate topping)

(define subst2
  (lambda (new o1 o2 lat)
    (cond
     ((null? lat) '())
     ((or (eq? (car lat) o1)
	  (eq? (car lat) o2))
      (cons new (cdr lat)))
     (else
      (cons (car lat) (subst2 new o1 o2 (cdr lat)))))))

(define multi-remove-member
  (lambda (a lat)
    (cond
     ((null? lat) '())
     ((eq? (car lat) a)
      (multi-remove-member a (cdr lat)))
     (else
      (cons (car lat) (multi-remove-member a (cdr lat)))))))

(define multi-insertR
  (lambda (new old lat)
    (cond
     ((null? lat)
      '())
     ((eq? (car lat) old)
      (cons old (cons new (multi-insertR new old (cdr lat)))))
     (else
      (cons (car lat) (multi-insertR new old (cdr lat)))))))

(define multi-insertL
  (lambda (new old lat)
    (cond
     ((null? lat)
      '())
     ((eq? (car lat) old)
      (cons new (cons old (multi-insertL new old (cdr lat)))))
     (else
      (cons (car lat) (multi-insertL new old (cdr lat)))))))


(define multi-subst
  (lambda (new old lat)
    (cond
     ((null? lat) '())
     ((eq? (car lat) old) (cons new (multi-subst new old (cdr lat))))
     (else
      (cons (car lat) (multi-subst new old (cdr lat)))))))

;; 4. Numbers games
(define add1
  (lambda (n)
    (+ n 1)))

(define subtract1
  (lambda (n)
    (- n 1)))

;; use add1 to build a number
;; (o+ 3 5)
;; reduce m until zero and call add1 for each, apply n when m is zero
;; (add1 (add1 (add1 (add1 (add1 3)))))
(define o+
  (lambda (n m)
    (cond
     ((zero? m) n)
     (else
      (add1 (o+ n (subtract1 m)))))))

;; (o+ 3 5) 8
;; (add1 (add1 (add1 (add1 (add1 3))))) 8 

(define o-
  (lambda (n m)
    (cond
     ((zero? m) n)
     (else
      (subtract1 (o- n (subtract1 m)))))))

;;
;; (o- 12 10) 2
;; (sub1 (sub1 (sub1 (sub1 (sub1 (sub1 (sub1 (sub1 (sub1 (sub1 12)))))))))) 2

(define addtup
  (lambda (tup)
    (cond
     ((null? tup) 0)
     (else
      (o+ (car tup) (addtup (cdr tup)))))))

(define o*
  (lambda (n m)
    (cond
     ((zero? m) 0)
     (else
      (o+ n (o* n (sub1 m)))))))

;;(o* 10 3)
;;(o+ 10
;;    (o+ 10
;;	(o+ 10 0)))

(define tup+
  (lambda (tup1 tup2)
    (cond
     ((null? tup1) tup2)
     ((null? tup2) tup1)
     (else
      (cons (o+ (car tup1) (car tup2)) (tup+ (cdr tup1) (cdr tup2)))))))

;; remember list construction:
;; use cons to build lists
(cons 1 (cons 2 (cons 3 '()))) ; (1 2 3)

(cons (o+ 1 1)
      (cons (o+ 2 2)
	    (cons (o+ 3 3) '(4 5)))) ; (2 4 6 4 5)

(define o>
  (lambda (n m)
    (cond
     ((zero? n) #f)
     ((zero? m) #t)
     (else
      (o> (sub1 n) (sub1 m))))))

(define o<
  (lambda (n m)
    (cond
     ((zero? m) #f)
     ((zero? n) #t)
     (else
      (o< (sub1 n) (sub1 m))))))

(define o=
  (lambda (n m)
    (cond
     ((o> n m) #f)
     ((o< n m) #f)
     (else
      #t))))

(define o^
  (lambda (n m)
    (cond
     ((zero? m) 1)
     (else
      (o* n (o^ n (sub1 m)))))))

(define o/
  (lambda (n m)
    (cond
     ((o< n m) 0)
     (else
      (add1 (o/ (o- n m) m))))))

;; add1 builds numbers, recur subtracting of n from m until zero
;; (o/ 15 4) = 1 + (o/ 11 4)
;;                   = 1 + (1 + (o/ 7 4)
;;                   = 1 + (1 + (1 + (o/ 3 4)
;;                   = 1 + (1 + (1 + (1 + 0)))

(define length
  (lambda (lat)
    (cond
     ((null? lat) 0)
     (else
      (add1 (length (cdr lat)))))))

(define pick
  (lambda (n lat)
    (cond
     ((zero? (sub1 n))
      (car lat))
     (else
      (pick (sub1 n) (cdr lat))))))

(define rempick
  (lambda (n lat)
    (cond
     ((one? n) (cdr lat))
     (else
      (cons (car lat) (rempick (sub1 n) (cdr lat)))))))

(define no-nums
  (lambda (lat)
    (cond
     ((null? lat) '())
     (else
      (cond
       ((number? (car lat)) (no-nums (cdr lat)))
       (else
	(cons (car lat) (no-nums (cdr lat)))))))))

(define all-nums
  (lambda (lat)
    (cond
     ((null? lat) '())
     (else
      (cond
       ((number? (car lat)) (cons (car lat)
				  (all-nums (cdr lat))))
       (else
	(all-nums (cdr lat))))))))

(define eqan?
  (lambda (a1 a2)
    (cond
     ((and (number? a1) (number? a2)) (= a1 a2))
     ((or (number? a1) (number? a2)) #f)
     (else
      (eq? a1 a2)))))

(define occur
  (lambda (a lat)
    (cond
     ((null? lat) 0)
     (else
      (cond ((eq? (car lat) a)
	     (add1 (occur a (cdr lat))))
	    (else
	     (occur a (cdr lat))))))))

(define one?
  (lambda (n)
    (= n 1)))

;; 5. *Oh My Gawd*: it's full of stars
;; A *-function recurs on the car as well as the cdr
;; *-functions work on lists that are either
;; -- empty,
;; -- an atom consed onto a list, or
;; -- a list conses onto a list

(define remove-member*
  (lambda (a l)
    (cond
     ((null? l) '())
     ((atom? (car l))
      (cond
       ((eq? (car l) a) (remember* a (cdr l)))
       (else
	(cons (car l) (remember* a (cdr l))))))
     (else
      (cons (remember* a (car l)) (remember* a (cdr l)))))))

(define insertR*
  (lambda (new old l)
    (cond
     ((null? l) '())
     ((atom? (car l))
      (cond
       ((eq? (car l) old)
	(cons old (cons new (insertR* new old (cdr l)))))
       (else
	(cons (car l) (insertR* new old (cdr l))))))
     (else
      (cons (insertR* new old (car l)) (insertR-* new old (cdr l)))))))

(define occur*
  (lambda (a l)
    (cond
     ((null? l) 0)
     ((atom? (car l))
      (cond
       ((eq? (car l) a)
	(add1 (occur* a (cdr l))))
       (else
	(occur* a (cdr l)))))
     (else (o+ (occur* a (car l))
	       (occur* a (cdr l)))))))

(define substitute*
  (lambda (new old l)
    (cond
     ((null? l) '())
     ((atom? (car l))
      (cond
       ((eq? (car l) old)
	(cons new (substitute* new old (cdr l))))
       (else
	(cons (car l) (substitute* new old (cdr l))))))
     (else
      (cons (substitute* new old (car l))
	    (substitute* new old (cdr l)))))))

(define insertL*
  (lambda (new old l)
    (cond
     ((null? l) '())
     ((atom? (car l))
      (cond
       ((eq? (car l) old)
	(cons new
	      (cons old (insertL* new old (cdr l)))))
       (else
	(cons (car l) (insertL* new old (cdr l))))))
     (else
      (cons (insertL* new old (car l))
	    (insertL* new old (cdr l)))))))

(define member*
  (lambda (a l)
    (cond
     ((null? l) #f)
     ((atom? (car l))
      (or (eq? (car l) a)
	  (member* a (cdr l))))
     (else
      (or (member* a (car l))
	  (member* a (cdr l)))))))

(define leftmost
  (lambda (l)
    (cond
     ((atom? (car l)) (car l))
     (else
      (leftmost (car l))))))

;; eqlist -- a function that determines if two lists are equal
;; "Each argument may be wither
;; -- empty,
;; -- an atom consed onto a list
;; for example, at the same tine as the first argument may be the empty list,
;; the scond argument could be the empty list or have an atom or a list in the car position

(define equal?
  (lambda (s1 s2)
    (cond
     ((and (atom? s1) (atom? s2))
      (eqan? s1 s2))
     ((or (atom? s1) (atom? s2))
      #f)
     (else
      (eqlist? s1 s2)))))

(define eqlist?
  (lambda (l1 l2)
    (cond
     ((and (null? l1) (null? l2)) #t)
     ((or (null? l1) (null? l2)) #f)
     (else
      (and (equal? (car l1) (car l2))
	   (eqlist? (cdr l1) (cdr l2)))))))

;; simplify only after the function is correct
(define remove-member2
  (lambda (s l)
    (cond
     ((null? l) '())
     ((atom? (car l))
	     (cond
	      ((equal? (car l) s) (cdr l))
	      (else
	       (cons (car l) (remove-member2 s (cdr l))))))
     (else
      (cond
       ((equal? (car l) s) (cdr l))
       (else
	(cons (car l) (remove-member2 s (cdr l)))))))))

;; simplify atom check (equals takes care of this
(define remove-member2
  (lambda (s l)
    (cond
     ((null? l) '())
     (else
      (cond
       ((equal? (car l) s) (cdr l))
       (else
	(cons (car l) (remove-member2 s (cdr l)))))))))

;; the inner cond asks questions the outer cond can ask
(define remove-member2
  (lambda (s l)
    (cond
     ((null? l) '())
     ((equal? (car l) s) (cdr l))
     (else
      (cons (car l) (remove-member2 s (cdr l)))))))

;; 6. Shadows
(define numbered?
  (lambda (aexp)
    (cond
     ((atom? aexp) (number? aexp))
     (else
      (and (numbered? (car aexp))
	   (numbered? (car (cdr (cdr aexp)))))))))

(define value
  (lambda (nexp)
    (cond
     ((atom? nexp) nexp)
     ((eq? (car (cdr nexp)) '+)
      (o+ (value (car nexp))
	  (value (car (cdr (cdr nexp))))))
     ((eq? (car (cdr nexp)) 'x)
      (o* (value (car nexp))
	  (value (car (cdr (cdr nexp))))))
     (else
      (o^ (value (car nexp))
	  (value (car (cdr (cdr nexp)))))))))

(value '(1 + 3))

(define 1st-sub-exp
  (lambda (aexp)
    (car (cdr aexp))))

(define 2nd-sub-exp
  (lambda (aexp)
    (car (cdr (cdr aexp)))))

(define operator
  (lambda (aexp)
    (car aexp)))

;; rewrite value with helpers
(define value
  (lambda (nexp)
    (cond
     ((atom? nexp) nexp)
     ((eq? (operator nexp) '+)
      (o+ (value (1st-sub-exp nexp))
	  (value (2nd-sub-exp nexp))))
     ((eq? (operator nexp) '*)
      (o* (value (1st-sub-exp nexp))
	  (value (2nd-sub-exp nexp))))
     (else
      (o^ (value (1st-sub-exp nexp))
	  (value (2nd-sub-exp nexp)))))))

;; can we represent one as (()) ?
(define sero?
  (lambda (n)
    (null? n)))

(define edd1
  (lambda (n)
    (cons '() n)))

(define zub1
  (lambda (n)
    (cdr n)))

(define o+
  (lambda (n m)
    (cond
     ((sero? m) n)
     (else
      (edd1 (o+ n (zub1 m)))))))

;; 7. Friends and Relations

(define set?
  (lambda (lat)
    (cond
     ((null? lat) #t)
     ((member? (car lat) (cdr lat)) #f)
     (else
      (set? (cdr lat))))))

(define makeset
  (lambda (lat)
    (cond
     ((null? lat) '())
     (else
      (cons (car lat) (makeset
		       (multi-remove-member (car lat) (cdr lat))))))))

(define subset?
  (lambda (set1 set2)
    (cond
     ((null? set1) #t)
     (else
      (and
       (member? (car set1) set2)
       (subset? (cdr set1) set2))))))

(define eqset?
  (lambda (set1 set2)
    (and (subset? set1 set2)
	 (subset? set2 set1))))

(define intersect?
  (lambda (set1 set2)
    (cond
     ((null? set1) #f)
     (else
      (or (member? (car set1) set2)
	  (intersect? (cdr set1) set2))))))

(define intersect
  (lambda (set1 set2)
    (cond
     ((null? set1) '())
     ((member? (car set1) set2)
      (cons (car set1)
	    (intersect (cdr set1) set2)))
     (else
      (intersect (cdr set1) set2)))))

(define union
  (lambda (set1 set2)
    (cond
     ((null? set1) set2)
     ((member? (car set1) set2)
      (union (cdr set1) set2))
     (else
      (cons (car set1)
	    (union (cdr set1) set2))))))

(define xxx
  (lambda (set1 set2)
    (cond
     ((null? set1) '())
     ((member? (car set1) set2)
      (xxx (cdr set1) set2))
     (else
      (cons (car set1)
	    (xxx (cdr set1) set2))))))

(define intersectall
  (lambda (l-set)
    (cond
     ((null? (cdr l-set)) (car l-set))
     (else
      (intersect (car l-set)
		 (intersectall (cdr l-set)))))))

					; example
(intersectall '((6 pears and)
		(3 peaches and 6 peppers)
		(8 pears and 6 plums)
		(and 6 prunes with some apples)))
					; (6 and)

					; recursive calls to intersect
(intersect '(6 pears and)
	   (intersect '(3 peaches and 6 peppers)
		      (intersect '(8 pears and 6 plums) '(and 6 prunes with some apples))))
					; (6 and)

(define a-pair?
  (lambda (x)
    (cond
     ((atom? x) #f)
     ((null? x) #f)
     ((null? (cdr x)) #f)
     ((null? (cdr (cdr x))) #t)
     (else
      #f))))

(define first
  (lambda (p)
    (car p)))

(define second
  (lambda (p)
    (car (cdr p))))

;; we could also use cdar
(define second
  (lambda (p)
    (cadr p)))

(define third
  (lambda (p)
    (car (cdr (cdr p)))))

(define build
  (lambda (s1 s2)
    (cons s1 (cons s2 '()))))

(define fun?
  (lambda (rel)
    (set? (firsts rel))))

(define revrel
  (lambda (rel)
    (cond
     ((null? rel) '())
     (else
      (cons (build
	     (second (car rel))
	     (first (car rel)))
	    (revrel (cdr rel)))))))

;; we can introduce a helper to aid readability

(define revpair
  (lambda (pair)
    (build (second pair) (first pair))))

(define revrel
  (lambda (rel)
    (cond
     ((null? rel) '())
     (else
      (cons
       (revpair (car rel)) (revrel (cdr rel)))))))

;;remember firsts? we can define seconds with second

(define seconds
  (lambda (l)
    (cond
     ((null? l) '())
     (else (cons (second (car l))
		 (seconds (cdr l)))))))

(seconds '((apple peach pumpkin)
	  (plum pear cherry)
	  (grape raisin pea)
	  (bean carrot eggplant))) ; (peach pear raisin carrot)

(define fullfun?
  (lambda (fun)
    (set? (seconds fun))))

;; another way to write fullfun?

(define one-to-one?
  (lambda (fun)
    (fun? (revrel fun))))

;; 8. Lambda the Ultimate
;; Introducing the power of abstraction

(define remember-f
  (lambda (test? a l)
    (cond
     ((null? l) '())
     ((test? (car l) a) (cdr l))
     (else
      (cons (car l)
	    (remember-f test? a (cdr l)))))))

;; A curried function returns a function with the argument bound
(lambda (a)
  (lambda (x)
    (eq? x a))) ;; returns (lambda (x) (eq? x a)) 

(define eq?-c
  (lambda (a)
    (lambda (x)
      (eq? x a))))

(define eq?-salad
  (eq?-c 'salad))

;; rewrite remember-f as a function of one argument test? that returns
;; an argument lime remember with eq? replaced by test

(define rember-f
  (lambda (test?)
    (lambda (a l)
      (cond
       ((null? l) '())
       ((test? (car l) a) (cdr l))
       (else
	(cons (car l) ((rember-f test?) a (cdr l))))))))

(define insertL-f
  (lambda (test?)
    (lambda (new old l)
      (cond
       ((null? l) '())
       ((test? (car l) old)
	(cons new (cons old (cdr l))))
       (else
	(cons (car l) ((insertL-f test?) new old (cdr l))))))))

(define insertR-f
  (lambda (test?)
    (lambda (new old l)
      (cond
       ((null? l) '())
       ((test? (car l) old)
	(cons old (cons new (cdr l))))
       (else
	(cons (car l) ((insertR-f test?) new old (cdr l))))))))

;; insertR-f and insertL-f only differ in the list consing, this can be expressed by passing in a function

(define seqL
  (lambda (new old l)
    (cons new (cons old l))))

(define seqR
  (lambda (new old l)
    (cons old (cons new l))))

(define insert-g
  (lambda (seq)
    (lambda (new old l)
      (cond
       ((null? l) '())
       ((eq? (car l) old)
	(seq new old (cdr l)))
       (else
	(cons (car l) ((insert-g seq) new old (cdr l))))))))

(define insertL (insert-g seqL))

(define insertR (insert-g seqR))

;; seqL and seqR don't need to be defined. Their definitions can be passed instead.
;; This is better because you don't need to remember as many names
(define insertL
  (insert-g
   (lambda (new old l)
     (cons new (cons old l)))))

(define insertR
  (insert-g
   (lambda (new old l)
     (cons old (cons new l)))))

(define subst
  (lambda (new old l)
    (cond
     ((null? l) '())
     ((eq? (car l) old)
      (cons new (cdr l)))
     (else
      (cons (car l)
	    (subst new old (cdr l)))))))

(define seqS
  (lambda (new old l)
    (cons new l)))

(define subst (insert-g seqS))

;; redefinng rember with a HOF

;; seqrem doesn't require a parameter for an old token but insert-g expects to call
;; the passed in function with three parameters. #f is a good token to stand in place, but #t or a quoted token would have the same effect
(define yyy
  (lambda (a l)
    ((insert-g seqrem) #f a l)))

(define seqrem
  (lambda (new old l)
    l))

;; the ninth commandment
;; abstract common patterns with a new function

;; write the value function from chapter 6 using a helper function, atom-to-function which
;; 1. Takes one argument x and
;; 2. returns the function o+
;;         if (eq? x (quote +))
;;     returns the function o*
;;         if (eq x (quote x)) and
;;     returns the function o^ otherwise

(define atom-to-function
  (lambda (x)
    (cond
     ((eq? x '+) o+)
     ((eq? x 'x) o*)
     ((eq? x '^) o^)
     (else #f))))

(define value
  (lambda (nexp)
    (cond
     ((atom? nexp) nexp)
     (else
      ((atom-to-function (operator nexp))
       (value (1st-sub-exp nexp))
       (value (2nd-sub-exp nexp)))))))

(define multirember-f
  (lambda (test?)
    (lambda (a lat)
      (cond
       ((null? lat) '())
       ((test? a (car lat))
	((multirember-f test?) a (cdr lat)))
       (else
	(cons (car lat)
	      ((multirember-f test?) a (cdr lat))))))))

(define multirember-eq?
  (multirember-f eq?))

(define eq?-tuna
  (eq?-c 'tuna))

;; instead of taking test? and returning a function, multiremberT takes
;; a function like eq?-tuna and a lat and then does its work
(define multiremberT
  (lambda (test? lat)
    (cond
     ((null? lat) '())
     ((test? (car lat))
      (multiremberT test? (cdr lat)))
     (else
      (cons (car lat) (multiremberT test? (cdr lat)))))))

;; we introduce a collector,
;; for every atom of the lat see whether it is eq? to a.
;; if not, the values are collected into one list ls1;
;; the others for which the answer is true are collected in a second list ls2.
;; Finally, it determines the value of (fs ls1 ls2)
(define multirember&co
  (lambda (a lat col)
    (cond
     ((null? lat)
      (col '() '()))
     ((eq? (car lat) a)
      (multirember&co a (cdr lat)
		      (lambda (newlat seen)
			  (col newlat
			       (cons (car lat) seen)))))
     (else
      (multirember&co a (cdr lat)
		      (lambda (newlat seen)
			(col
			 (cons (car lat) newlat) seen)))))))

;; this is a tricky one
;; col is replaced with the lambda definition and wrapped around a new function. A simpler example:
;;
;; (define succ2
;; 	  (lambda (s)
;; 	    ((lambda (s)
;; 	       ((lambda (x) x)  (+ 1 s))) 
;; 	     (+ 1 s))))
;; (succ2 2) ; 4
;;
;;when its terminated the function is reduced
;;applying the terminating value first and then calling successor functions
;;
;;this is called a continuation and is known as continuating passing style
;;
  
(define a-friend
  (lambda (x y)
    (null? y)))

(define new-friend
  (lambda (newlat seen)
    (a-friend newlat
	 (cons 'tuna seen))))

(define last-friend
  (lambda (x y)
    (length x)))

;; The Tenth Commandment
;; Build functions to collect more than one value at a time

(define multiinsertLR
 (lambda (new oldL oldR lat)
   (cond
    ((null? lat) '())
    ((eq? (car lat) oldL)
     (cons new
	   (cons oldL (multiinsertLR new oldL oldR (cdr lat)))))
    ((eq? (car lat) oldR)
     (cons oldR
	   (cons new (multiinsertLR new oldL oldR (cdr lat)))))
    (else
     (cons (car lat)
	   (multiinsertLR new oldL oldR (cdr lat)))))))

;; adding a collector function to multiinsertLR requires a new argument
;; remember, '() '() is the natural recursion for col
(define multiinsertLR&co
 (lambda (new oldL oldR lat col)
   (cond
    ((null? lat) (col '() 0 0))
    ((eq? (car lat) oldL)
     (multiinsertLR&co new oldL oldR
		       (cdr lat)
		       (lambda (newlat L R)
			 (col (cons new (cons oldL newlat))
			      (add1 L) R))))
     
    ((eq? (car lat) oldR)
     (multiinsertLR&co new oldL oldR
		       (cdr lat)
		       (lambda (newlat L R)
			 (col (cons oldR (cons new newlat))
			      L (add1 R)))))
    (else
     (multiinsertLR&co new oldL oldR
		       (cdr lat)
		       (lambda (newlat L R)
			 (col (cons (car lat) newlat) L R)))))))

(define evens-only*
  (lambda (l)
    (cond
     ((null? l) '())
     ((atom? (car l))
      (cond
       ((even? (car l))
	(cons (car l)
	      (evens-only* (cdr l))))
       (else
	(evens-only* (cdr l)))))
     (else
      (cons (evens-only* (car l))
	    (evens-only* (cdr l)))))))

(define evens-only*&co
  (lambda (l col)
    (cond
     ((null? l) (col '() 1 0))
     ((atom? (car l))
      (cond
       ((even? (car l))
	(evens-only*&co (cdr l)
			(lambda (newl p s)
			  (col (cons (car l) newl) (* (car l) p) s))))
       (else
	(evens-only*&co (cdr l)
			(lambda (newl p s)
			  (col newl p (+ (car l) s)))))))
     
      (else
       (evens-only*&co (car l)
		       (lambda (al ap as)
			 (evens-only*&co (cdr l)
					 (lambda (dl dp ds)
					   (col (cons al dl)
						(* ap dp)
						(+ as ds))))))))))

(define the-last-friend
  (lambda (newl product sum)
    (cons sum (cons product newl))))

;; an example continuation
(evens-only*&co '((9 1 2 8) 3 10 ((9 9) 7 6) 2) the-last-friend)
					; (38 1920 (2 8) 10 (() 6) 2)

;; 9. ... and Again, and Again, and Again, ...

(define looking
  (lambda (a lat)
    (keep-looking a (pick 1 lat) lat)))

;; given a symbol to check for and a starting value, recur on lat unitl a symbol is found and compare
;; if a number is found the process repeats with the element at that position as the starting value
(define keep-looking
  (lambda (a sorn lat)
    (cond
     ((number? sorn)
      (keep-looking a (pick sorn lat) lat))
     (else
      (eq? sorn a)))))

;; keep-looking is unusual, it does not recur on a part of lat
;; from all available evidence it appears to get closer to it's goal, but not always
;; if a list is a tup it will never stop looking -- functions like keep-looking are called partial functions
;; so far all the functions we have defined always return a value -- they are called total functions


;; eternity is the most partial function
(define eternity
  (lambda (x)
    (eternity x)))

;; the function shift takes a air whose first component is a pair
;; and buids a pair by shifting the second part of the first component
;; into the second component
(define shift
  (lambda (pair)
    (build (first (first pair))
	   (build (second (first pair))
		  (second pair)))))

(shift '((a b) c))
;;(a (b c))


;; like keep-looking, align changes its arguments for their recursive usees but in
;; neither case is the chage guaranteed ti get us closer to the goal
(define align
  (lambda (pora)
    (cond
     ((atom? pora) pora)
     ((a-pair? (first pora))
      (align (shift pora)))
     (else
      (build (first pora)
	     (align (second pora)))))))

;; recursive arguments to align are not smaller then the original one, this violates the seventh commandment:
;; Recur on the subparts that are of the same nature:
;; - On the sublists of a list
;; - On the subexpressions of an arithmetic expression

(align '((a b) c))
;; (a (b c))

;; count the number of atoms in aligns arguments
(define length*
  (lambda (pora)
    (cond
     ((atom? pora) 1)
     (else
      (+ (length* (first pora))
	 (length* (second pora)))))))

;; length* isn't the right function for determining the length of the argument
;; a better function will pay more attention to the first component

;; we can use weight* to analyse aligns components
;; the arguments do not change but the first component is simpler.
(define weight*
  (lambda (pora)
    (cond
     ((atom? pora) 1)
     (else
      (+ (* (weight* (first pora)) 2)
	 (weight* (second pora)))))))

;; is align partial?
;; No, align yields a value for evert argument

;; shuffle is like align but uses revpair, instead of shift
(define shuffle
  (lambda (pora)
    (cond
     ((atom? pora) pora)
     ((a-pair? (first pora))
      (shuffle (revpair pora)))
     (else
      (build (first pora)
	     (shuffle (second pora)))))))

;; shuffle is not total because if pora is two pairs shuffle will swap over and over
(shuffle '((a b) c))
;;(c (a b))
;;(shuffle '((a b) (c d)))
;; must be interrupted

(define C
  (lambda (n)
    (cond
     ((one? n) 1)
     (else
      (cond
       ((even? n)
	(C (/ n 2)))
       (else
	(C (add1 (* 3 n)))))))))

;; is C total?
;; the Collatz conjecture is that no matter what value of n, the sequence will always reach 1
;; all evidence and heurists show this to be true, but no provable truth exists

(define A
  (lambda (n m)
    (cond
     ((zero? n) (add1 m))
     ((zero? m) (A (sub1 n) 1))
     (else
      (A (sub1 n)
	 (A n (sub1 m)))))))

;; the akermann function is total. A's arguments like shuffle and looking do not necessarily decrease for the recursion
;; although total its value grows rapidly, A may return a value so late that it is too late.

;; (A 1 2); 4
;; (A 4 3); we can't possibly calculate this value

;; can we write a function that tells us whether some function returns with a value for every argument?
;;   no, there is no way around the halting problem (Turing, Godel)

(define length
  (lambda (l)
    (cond
     ((null? l) 0)
     (else
      (add1 (length (cdr l)))))))

;; if we didn't have define? could we still define length?

((lambda (l)
  (cond
   ((null? l) 0)
   (else
    (add1 (eternity (cdr l))))))  '()) ; 0

;; this will give us the length of an empty list, anything else will run forever
;; what about length <= 1?
;; if we replace eternity with the definition above it will evaluate for a list of 1

((lambda (l)
   (cond
   ((null? l) 0)
   (else
    (add1 ((lambda (l)
	     (cond
	      ((null? l) 0)
	      (else
	       (add1 (eternity (cdr l))))))
	   (cdr l))))))
 '(1)); 1

;; we can apply the same reasoning to get us length<=2

((lambda (l)
   (cond
   ((null? l) 0)
   (else
    (add1 ((lambda (l)
	     (cond
	      ((null? l) 0)
	      (else
	       (add1 ((lambda (l)
			(cond
			 ((null? l) 0)
			 (else
			  (add1 (eternity (cdr l)))))) (cdr l))))))
	   (cdr l))))))
 '(1 2))

;; if we could write an infinite function in this style we could write length∞ to determine the length of any list we make
;; first we need to abstract the function that makes length
((lambda (mk-length)
   (mk-length eternity))
 (lambda (length)
   (lambda (l)
     (cond
      ((null? l) 0)
      (else
       (add1 (length (cdr l))))))))

;; length <=1
(((lambda (mk-length)
    (mk-length
     (mk-length eternity)))
 (lambda (length)
   (lambda (l)
     (cond
      ((null? l) 0)
      (else
       (add1 (length (cdr l)))))))) '(1))

;; length <=2
(((lambda (mk-length)
    (mk-length
     (mk-length
      (mk-length eternity))))
 (lambda (length)
   (lambda (l)
     (cond
      ((null? l) 0)
      (else
       (add1 (length (cdr l)))))))) '(1 2))

;; mk-length is a far more equal name than length. it is a constant reminder that the first
;; argument to mk-length is mk-length
;; if we pass mk-length to itself we create an additional recursive use
(((lambda (mk-length)
   (mk-length mk-length))
 (lambda (mk-length)
   (lambda (l)
     (cond
      ((null? l) 0)
      (else
       (add1 (mk-length (cdr l))))))))
 '())

;; the length function
(((lambda (mk-length)
   (mk-length mk-length))
 (lambda (mk-length)
   (lambda (l)
     (cond
      ((null? l) 0)
      (else
       (add1 ((mk-length mk-length) (cdr l))))))))
 '(1 2 3))

;; lets return the function that looks like length by extracting the application of mk-length to itself
(((lambda (mk-length)
   (mk-length mk-length))
 (lambda (mk-length)
   ((lambda (length)
      (lambda (l)
	(cond
	 ((null? l) 0)
	 (else
	  (add1 (length (cdr l)))))))
    (lambda (x)
      ((mk-length mk-length) x)))))
 '(1 2 3 4)) 

;; the function that looks like length can be further extracted, it doesn't depend on mk-length at all
(((lambda (le)
    ((lambda (mk-length)
       (mk-length mk-length))
     (lambda (mk-length)
       (le
	(lambda (x)
	  ((mk-length mk-length) x))))))
  (lambda (length)
	  (lambda (l)
	    (cond
	     ((null? l) 0)
	     (else
	      (add1 (length (cdr l))))))))
 '(1 2 3 4 5))

;; here is the function that makes length
(lambda (le)
    ((lambda (mk-length)
       (mk-length mk-length))
     (lambda (mk-length)
       (le
	(lambda (x)
	  ((mk-length mk-length) x))))))

;; now we can give its name; this function is the applicative-order Y combinator
(define Y
  (lambda (le)
    ((lambda (f)
       (f f))
     (lambda (f)
       (le
	(lambda (x)
	  ((f f) x)))))))

((Y (lambda (length)
      (lambda (l)
	(cond
	 ((null? l) 0)
	 (else
	  (add1 (length (cdr l))))))))
 '(1 2 3 4 5 6))
