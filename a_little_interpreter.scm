(define build
  (lambda (s1 s2)
    (cons s1 (cons s2 '()))))

(define first
  (lambda (p)
    (car p)))

(define second
  (lambda (p)
    (cadr p)))

(define thrid
  (lambda (p)
    (caddr p)))

(define new-entry build)

;; an entry is a pair of lists whose first list is a set
;; the two list must be of equal length
'((appetizer entree beverage)
 (pate boeuf vin))

'((appetizer entree beverage)
  (beer beer beer))

'((beverge dessert)
  ((food is) (number one with us)))

(define new-entry build)

;; we can use new-entry to build an entry, a set of keys and their associated value
(new-entry '(appetizer entree beverage)
	   '(pate boeuf vin))

;; lookup for entries
(define lookup-in-entry
  (lambda (name entry entry-f)
    (lookup-in-entry-help name
			  (first entry)
			  (second entry)
			  entry-f)))

(define lookup-in-entry-help
  (lambda (name names values entry-f)
    (cond
     ((null? names) (entry-f name))
     ((eq? (car names) name)
      (car values))
     (else
      (lookup-in-entry-help name
			    (cdr names)
			    (cdr values)
			    entry-f)))))

(lookup-in-entry 'entree '((appetizer entree beverage)
			   (food tastes good))
		 (lambda (name)
		   #f))

;; a table is a list of entries
'()
					; empty table
'(((appetizer entree beverage)
   (pate boeuf vin))
  ((beverage dessert)
   ((food is) (number one with us))))
					;table with two entires

(define extend-table cons)

(extend-table '((appetizer entree beverage)
		(pate boeuf vin)) '())

;; lookup for tables
(define lookup-in-table
  (lambda (name table table-f)
    (cond
     ((null? table) (table-f name))
     (else
      (lookup-in-entry name
		       (car table)
		       (lambda (name)
			 (lookup-in-table name
					  (cdr table)
					  table-f)))))))

(lookup-in-table 'entree '(((entree dessert)
			    (spaghetti spumoni))
			   ((appetizer entree beverage)
			    (food tastes good)))
		 (lambda (name)
		   #f))

(define expression-to-action
  (lambda (e)
    (cond
     ((atom? e) (atom-to-action e))
     (else (list-to-action e)))))

(define atom-to-action
  (lambda (e)
    (cond
     ((number? e) *const)
     ((eq? e #t) *const)
     ((eq? e #f) *const)
     ((eq? e 'cons) *const)
     ((eq? e 'car) *const)
     ((eq? e 'cdr) *const)
     ((eq? e 'null?) *const)
     ((eq? e 'eq?) *const)
     ((eq? e 'atom?) *const)
     ((eq? e 'zero?) *const)
     ((eq? e 'add1) *const)
     ((eq? e 'sub1) *const)
     ((eq? e 'number?) *const)
     (else
      *identifier))))

(define list-to-action
  (lambda (e)
    (cond
     ((atom? (car e))
      (cond
       ((eq? (car e) 'quote) *quote)
       ((eq? (car e) 'lambda) *lambda)
       ((eq? (car e) 'cond) *cond)
       (else
	*application)))
     (else
      *application))))

;; using value and meaning we can approximate the Scheme (and Lisp) function eval
;; the '() represents the empty table
;; the function value, together with all the functions it uses, is called an interpreter
(define value
  (lambda (e)
    (meaning e '())))

(define meaning
  (lambda (e table)
    ((expression-to-action e) e table)))

(meaning 5 '())


;; define the actions

(define *const
  (lambda (e table)
    (cond
     ((number? e) e)
     ((eq? e #t) #t)
     ((eq? e #f) #f)
     (else
      (build 'primitive e)))))

(define *quote
  (lambda (e table)
    (text-of e)))

(define text-of second)

;; so far we have passed in a table to each function but have not used it
;; we need a table to remember the values of identifiers

(define *identifier
  (lambda (e table)
    (lookup-in-table e table initial-table)))

(define initial-table
  (lambda (name)
    (car '())))

;; the value of (lambda (x) x)

(define *lambda
  (lambda (e table)
    (build 'non-primitive
	   (cons table (cdr e)))))

;; table, formals, body
;; (non-primitive ( (((y z)((8) 9))) (x) (cons x y))

(define table-of first)
(define formals-of second)
(define body-of third)

;; (cond .. ) is a special form that takes any number of cond-lines

(define evcon
  (lambda (lines table)
    (cond
     ((else? (question-of (car lines)))
      (meaning (answer-of (car lines)) table))
     ((meaning (question-of (car lines)) table)
      (meaning (answer-of (car lines)) table))
     (else
      (evcon (cdr lines) table)))))

(define else?
  (lambda (x)
    (cond
     ((atom? x) (eq? x 'else))
     (else
      #f))))

(define question-of first)
(define answer-of second)

(define *cond
  (lambda (e table)
    (evcon (cond-lines-of e) table)))

(define cond-lines-of cdr)

(*cond '(cond (coffee klatsch) (else party))
       '(((coffee) (#t))
	 ((klatsch party) (5 (6)))))

(car (car (cdr '(cond (coffee klatsch) (else party)))))

(meaning 'coffee '(((coffee) (#t))
		   ((klatsch party) (5 (6)))))

(meaning 'klatsch '(((coffee) (#t))
		   ((klatsch party) (5 (6)))))

(*cond '(cond (coffee klatsch) (else party))
       '(((coffee) (#f))
	 ((klatsch party) (5 (6)))))

(meaning 'party '(((coffee) (#t))
		  ((klatsch party) (5 (6)))))

(define evlis
  (lambda (args table)
    (cond
     ((null? args) '())
     (else
      (cons (meaning (car args) table)
	    (evlis (cdr args) table))))))

(evlis '(car 5 '()) '())
;; ((primitive car) 5 ())

(define function-of car)
(define arguments-of cdr)

(define *application
  (lambda (e table)
    (apply
     (meaning (function-of e) table)
     (evlis (arguments-of e) table))))

;; a function has two representations
;; (primitive primitive-name) and
;; (non-primitive (table formals body))
;; the list (table formals body) is called a closure record

(define primitive?
  (lambda (l)
    (eq? (first l) 'primitive)))

(define non-primitive?
  (lambda (l)
    (eq? (first l) 'non-primitive)))

;; apply approximates the function apply available in Scheme and Lisp
(define apply
  (lambda (fun vals)
    (cond
     ((primitive? fun) (apply-primitive (second fun) vals))
     ((non-primitive? fun) (apply-closure (second fun) vals)))))

(define apply-primitive
  (lambda (name vals)
    (cond
     ((eq? name 'cons)
      (cons (first vals) (second vals)))
     ((eq? name 'car)
      (car (first vals)))
     ((eq? name 'cdr)
      (cdr (first vals)))
     ((eq? name 'null?)
      (null? (first vals)))
     ((eq? name 'eq?)
      (eq? (first vals) (second vals)))
     ((eq? name 'atom?)
      (:atom? (first vals)))
     ((eq? name 'zero?)
      (zero? (first vals)))
     ((eq? name 'add1)
      (add1 (first vals)))
     ((eq? name 'sub1)
      (sub1 (first vals)))
     ((eq? name 'number?)
      (number? (first vals))))))

(define :atom?
  (lambda (x)
    (cond
     ((atom? x) #t)
     ((null? x) #f)
     ((eq? (car x) 'primitive) #t)
     ((eq? (car x) 'non-primitive) #t)
     (else
      #f))))

(value '(car '(1 2 3)))

(apply '(primitive car) '((3 2)))

;; how do we evaluate ((lambda (x y) (cons x y)) 1 (2)) ?
;;
(meaning '(cons x y) '(((x y)
		  (1 (2)))))

(define apply-closure
  (lambda (closure vals)
    (meaning (body-of closure)
	     (extend-table
	      (new-entry
	       (formals-of closure) vals) (table-of closure)))))

(value '((lambda (x y) (cons x y)) 1 '(2)))

;; it's time for a banquet
